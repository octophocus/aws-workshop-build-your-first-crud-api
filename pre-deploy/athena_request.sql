--Request to help for the creation of policiy Iam of the CI user. This request show all awsapicall denied access.--

SELECT from_iso8601_timestamp(eventTime) AS "Time", useridentity.arn AS "Identity ARN", eventID AS "Event ID",
         eventsource AS "Service", eventname AS "Action", errorCode AS "Error", errorMessage AS "Message"
FROM my-table
WHERE from_iso8601_timestamp(eventtime) >= from_iso8601_timestamp('2021-10-29T06:40:00Z')
        AND from_iso8601_timestamp(eventtime) < from_iso8601_timestamp('2021-11-01T06:55:00Z')
        AND eventType = 'AwsApiCall'
        AND errorCode is not null
        AND (lower(errorCode) LIKE '%accessdenied%' OR lower(errorCode) LIKE '%unauthorized%')
ORDER BY eventTime desc
