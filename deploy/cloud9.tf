resource "aws_cloud9_environment_ec2" "main" {
  instance_type = "t2.nano"
  name          = "${var.prefix}-cloud9"
  owner_arn     = "arn:aws:iam::829196068745:user/Damien"

  tags = local.common_tags
}

data "aws_instance" "cloud9_instance" {
  filter {
    name = "tag:aws:cloud9:environment"
    values = [
    aws_cloud9_environment_ec2.main.id]
  }
}
