output "base_url" {
  value = aws_apigatewayv2_api.main.api_endpoint
}

output "cloud9_url" {
  value = "https://${var.region}.console.aws.amazon.com/cloud9/ide/${aws_cloud9_environment_ec2.main.id}"
}