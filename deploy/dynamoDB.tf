resource "aws_dynamodb_table" "main" {
  name           = "${var.prefix}-table"
  billing_mode   = "PAY_PER_REQUEST"
  write_capacity = 10
  read_capacity  = 10
  hash_key       = "id"

  attribute {
    name = "id"
    type = "S"
  }

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}