resource "aws_iam_role" "assume_role_lambda" {
  name = "${var.prefix}LambdaAssumeRole"

  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "lambda.amazonaws.com"
          },
          "Effect" : "Allow",
        }
      ]
    }
  )
}

data "aws_iam_policy_document" "lambda_role" {
  statement {
    sid = "PermissionsForLambdaToAccessDynamoDBtable"

    actions = [
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:Scan",
      "dynamodb:UpdateItem",
    ]

    resources = [
      "arn:aws:dynamodb:us-east-1:829196068745:table/${var.prefix}-table",
    ]
  }
  statement {
    sid = "PermissionsForLambdaToCreateAndPutLogCloudWatch"

    actions = [
      "logs:CreateLogGroup",
    ]

    resources = [
      "arn:aws:logs:us-east-1:829196068745:*",
    ]
  }

  statement {

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:us-east-1:829196068745:log-group:/aws/lambda/${var.prefix}-lambda_function:*"
    ]
  }


}

resource "aws_iam_policy" "lambda_role" {
  name   = "lambdaRolePolicy"
  path   = "/"
  policy = data.aws_iam_policy_document.lambda_role.json
}