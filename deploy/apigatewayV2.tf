resource "aws_apigatewayv2_api" "main" {
  name          = "${var.prefix}-apiGwV2"
  description   = "API CRUD for AWS Workshop"
  protocol_type = "HTTP"
  target        = aws_lambda_function.main.arn

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

resource "aws_apigatewayv2_integration" "itemsPUT" {
  api_id           = aws_apigatewayv2_api.main.id
  integration_type = "AWS_PROXY"

  connection_type        = "INTERNET"
  description            = " integration for awca API"
  integration_method     = "POST"
  integration_uri        = aws_lambda_function.main.invoke_arn
  payload_format_version = "2.0"
}

resource "aws_apigatewayv2_route" "itemsPUT" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "PUT /items"

  target = "integrations/${aws_apigatewayv2_integration.itemsPUT.id}"
}

resource "aws_apigatewayv2_integration" "itemsGET" {
  api_id           = aws_apigatewayv2_api.main.id
  integration_type = "AWS_PROXY"

  connection_type        = "INTERNET"
  description            = " integration for awca API"
  integration_method     = "POST"
  integration_uri        = aws_lambda_function.main.invoke_arn
  payload_format_version = "2.0"
}

resource "aws_apigatewayv2_route" "itemsGET" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "GET /items"

  target = "integrations/${aws_apigatewayv2_integration.itemsGET.id}"
}

resource "aws_apigatewayv2_integration" "itemsIdDELETE" {
  api_id           = aws_apigatewayv2_api.main.id
  integration_type = "AWS_PROXY"

  connection_type        = "INTERNET"
  description            = " integration for awca API"
  integration_method     = "POST"
  integration_uri        = aws_lambda_function.main.invoke_arn
  payload_format_version = "2.0"
}

resource "aws_apigatewayv2_route" "itemsIdDELETE" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "DELETE /items/{id}"

  target = "integrations/${aws_apigatewayv2_integration.itemsIdDELETE.id}"
}

resource "aws_apigatewayv2_integration" "itemsIdGET" {
  api_id           = aws_apigatewayv2_api.main.id
  integration_type = "AWS_PROXY"

  connection_type        = "INTERNET"
  description            = " integration for awca API"
  integration_method     = "POST"
  integration_uri        = aws_lambda_function.main.invoke_arn
  payload_format_version = "2.0"
}

resource "aws_apigatewayv2_route" "itemsIdGET" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "GET /items/{id}"

  target = "integrations/${aws_apigatewayv2_integration.itemsIdGET.id}"
}

resource "aws_apigatewayv2_stage" "main" {
  api_id      = aws_apigatewayv2_api.main.id
  name        = "prod"
  auto_deploy = true
  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gateway_main.arn
    format          = jsonencode({ "requestId" : "$context.requestId", "ip" : "$context.identity.sourceIp", "requestTime" : "$context.requestTime", "httpMethod" : "$context.httpMethod", "routeKey" : "$context.routeKey", "status" : "$context.status", "protocol" : "$context.protocol", "responseLength" : "$context.responseLength" })
  }

  depends_on = [
    aws_apigatewayv2_route.itemsGET,
    aws_apigatewayv2_route.itemsPUT,
    aws_apigatewayv2_route.itemsIdDELETE,
    aws_apigatewayv2_route.itemsIdGET,
  ]
}

resource "aws_apigatewayv2_deployment" "main" {
  api_id      = aws_apigatewayv2_api.main.id
  description = "deployment terraform"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_cloudwatch_log_group" "api_gateway_main" {
  name              = "/aws/apigateway/${var.prefix}-apiGwV2"
  retention_in_days = 7
}

resource "aws_apigatewayv2_domain_name" "main" {
  domain_name = "${var.dns_name}.${var.zone_dns}"

  domain_name_configuration {
    certificate_arn = aws_acm_certificate.awcaapi.arn
    endpoint_type   = "REGIONAL"
    security_policy = "TLS_1_2"
  }

  depends_on = [
    aws_acm_certificate_validation.awcaapi
  ]
}

resource "aws_apigatewayv2_api_mapping" "main" {
  api_id      = aws_apigatewayv2_api.main.id
  domain_name = aws_apigatewayv2_domain_name.main.id
  stage       = aws_apigatewayv2_stage.main.id
}