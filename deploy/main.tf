terraform {
  backend "s3" {
    bucket         = "aws-workshop-crud-api-tfstate-haken735"
    key            = "aws-workshop-crud-api.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "aws-workshop-crud-api-tf-state-lock"
  }

}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.63.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
  lambda_file_path = "function_src"
  Lambda_archive   = "lambda_function.zip"
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}