resource "aws_iam_role_policy_attachment" "lambda_execution" {
  role       = aws_iam_role.assume_role_lambda.name
  policy_arn = aws_iam_policy.lambda_role.arn

  depends_on = [
    aws_iam_role.assume_role_lambda,
    aws_iam_policy.lambda_role,
  ]
}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "function_src/index.js"
  output_path = "${local.lambda_file_path}/${local.Lambda_archive}"
}

resource "aws_lambda_function" "main" {
  filename      = "${local.lambda_file_path}/${local.Lambda_archive}"
  function_name = "${var.prefix}-lambda_function"
  role          = aws_iam_role.assume_role_lambda.arn
  handler       = "index.handler"
  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("${local.lambda_file_path}/${local.Lambda_archive}")
  runtime          = "nodejs14.x"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )

}

resource "aws_lambda_permission" "main" {
  statement_id  = "AllowAPIGatewaySample"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.main.arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.main.execution_arn}/*"
}