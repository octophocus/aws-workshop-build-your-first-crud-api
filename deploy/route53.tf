resource "aws_acm_certificate" "awcaapi" {
  domain_name       = "${var.dns_name}.${var.zone_dns}"
  validation_method = "DNS"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "awcaapi_acm_validation" {
  for_each = {
    for dvo in aws_acm_certificate.awcaapi.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  zone_id = var.zone_id
  name    = each.value.name
  type    = each.value.type
  ttl     = 60
  records = [
    each.value.record,
  ]

  allow_overwrite = true
}

resource "aws_acm_certificate_validation" "awcaapi" {
  certificate_arn         = aws_acm_certificate.awcaapi.arn
  validation_record_fqdns = [for record in aws_route53_record.awcaapi_acm_validation : record.fqdn]
}

resource "aws_route53_record" "awcaapi" {
  name    = aws_apigatewayv2_domain_name.main.domain_name
  type    = "A"
  zone_id = var.zone_id

  alias {
    name                   = aws_apigatewayv2_domain_name.main.domain_name_configuration[0].target_domain_name
    zone_id                = aws_apigatewayv2_domain_name.main.domain_name_configuration[0].hosted_zone_id
    evaluate_target_health = true
  }
}