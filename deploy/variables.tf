variable "project" {
  default = "aws-workshop-crud-api"
}

variable "contact" {
  default = "octophocus@gmail.com"
}

variable "prefix" {
  default = "awca"
}

variable "region" {
  default = "us-east-1"
}

variable "dns_name" {
  default = "awca-api"
}

variable "zone_dns" {
  default = "octophocus.click"
}

variable "zone_id" {
  default = "Z08235874S0RHG44YUF"
}